import gym
from stable_baselines3 import TD3
import numpy as np
from gym import spaces

class Env_residual(gym.Env):
    def __init__(self,env_real:gym.Env,agent:TD3):
        self.env_real = env_real
        self.agent = agent

        self.observation_space = env_real.observation_space
        #self.action_space = env_real.action_space
        len=1
        high = np.ones(len)
        low = (-1) * high
        self.action_space = spaces.Box(low=low, high=high, dtype=np.float32)

    def step(self, action):
        model_action= self.agent.predict(self.s0)
        #action_factor = (1+action)/2*1.5
        #applied_action = np.multiply(model_action[0],action_factor)
        action = (action)/2.+1.
        applied_action = model_action[0]*action
        #applied_action = model_action[0]+action
        #print(action_factor)
        return_state, return_reward, self.is_finalized, _ =self.env_real.step(applied_action)#step(applied_action)step(model_action[0])
        self.s0 = return_state
        return return_state,return_reward,self.is_finalized,{}


    def reset(self):
        self.s0 = self.env_real.reset()
        return self.s0


