import matplotlib.pyplot as plt
import numpy as np
from stable_baselines3 import TD3
from stable_baselines3.td3.policies import MlpPolicy
from stable_baselines3.common.noise import NormalActionNoise
from scipy.optimize import fmin_cobyla
from cpymad.madx import Madx

from residual_phy_rl.envs import env_awake_steering_simulated as awake_sim
from residual_phy_rl.envs import env_residual

OPTIONS = ['ECHO', 'WARN', 'INFO', 'DEBUG', 'TWISS_PRINT']
MADX_OUT = [f'option, -{ele};' for ele in OPTIONS]

madx = Madx()
madx.input('\n'.join(MADX_OUT))
madx.options.echo=False;madx.options.warn=True;

tt43_ini= "envs/electron_design.mad"

madx.call(file=tt43_ini,chdir=True)

madx.use(sequence= "tt43",range= '#s/plasma_merge')
# quads={}
# for ele, value in dict(madx.globals).items():
#         if 'kq' in ele:
#             quads[ele] = value*0.8#np.random.uniform(0.5,1.5,size=None)
#
# madx.globals.update(quads)

madx.input('initbeta0:beta0,BETX=5,ALFX=0,DX=0,DPX=0,BETY=5,ALFY=0,DY=0.0,DPY=0.0,x=0,px=0,y=0,py=0;')
twiss_cpymad =madx.twiss(beta0='initbeta0').dframe()

env = awake_sim.e_trajectory_simENV(twiss_cpymad)


def plot_results(env, label):

    rewards = env.rewards
    initial_states = env.initial_conditions

    iterations = []
    finals = []
    starts = []

    for i in range(len(rewards)):
        if (len(rewards[i]) > 0):
            finals.append(rewards[i][len(rewards[i]) - 1])
            starts.append(-np.sqrt(np.mean(np.square(initial_states[i]))))
            iterations.append(len(rewards[i]))

    plot_suffix = f', number of iterations: {env.TOTAL_COUNTER}'

    fig, axs = plt.subplots(2, 1, constrained_layout=True)

    ax=axs[0]
    ax.plot(iterations)
    ax.set_title('Iterations' + plot_suffix)
    fig.suptitle(label, fontsize=12)

    ax = axs[1]
    color = 'blue'
    ax.set_ylabel('Final RMS', color=color)
    ax.tick_params(axis='y', labelcolor=color)
    ax.plot(finals, color=color)
    ax.axhline(env.threshold, ls=':',c='r')
    ax.set_xlabel('Episodes ')


    color = 'lime'

    ax.plot(starts, color=color)

    plt.show()



if __name__ == '__main__':
    def objective(x):
        reward = env.step_opt(x)

        return (-1) * reward


    def constr1(x):
        return 1. - np.abs(x)


    is_train_first = False


    if (is_train_first):
        n_actions = env.action_space.shape[-1]
        action_noise = NormalActionNoise(mean=np.zeros(n_actions), sigma=0.2 * np.ones(n_actions))
        env.response_scale = 0.5
        env.recalculate_response()
        td3 = TD3(MlpPolicy, env, learning_starts=100, train_freq=10, action_noise=action_noise, verbose=1)
        td3.learn(total_timesteps=300, log_interval=10)
        td3.save('agents/awake_test_td3')
        plot_results(env, "")

    else:
        td3 = TD3.load('agents/awake_test_td3')
        env_residual = env_residual.Env_residual(env,td3)
        n_actions = env_residual.action_space.shape[-1]
        action_noise = NormalActionNoise(mean=np.zeros(n_actions), sigma=0.2 * np.ones(n_actions))

        env.MAX_TIME=15
        env.response_scale = 1
        env.recalculate_response()
        td3_residual = TD3(MlpPolicy, env_residual, learning_starts=15, train_freq=10, action_noise=action_noise, verbose=1)
        td3_residual.learn(total_timesteps=50, log_interval=10)
        plot_results(env, "")

