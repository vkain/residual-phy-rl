from cpymad.madx import Madx

from Application import twissReader
import numpy as np

OPTIONS = ['ECHO', 'WARN', 'INFO', 'DEBUG', 'TWISS_PRINT']
MADX_OUT = [f'option, -{ele};' for ele in OPTIONS]


madx = Madx()
madx.input('\n'.join(MADX_OUT))
madx.options.echo=False;madx.options.warn=True;

tt43_ini= "/opt/vkain/PycharmProjects/awakett43/2018/madx/electron_design.mad"

madx.call(file=tt43_ini,chdir=True)

madx.use(sequence= "tt43",range= '#s/plasma_merge')
quads={}
for ele, value in dict(madx.globals).items():
        if 'kq' in ele:
            quads[ele] = value*0.9#*np.random().uniform(0.9,1.1,1)

#madx.globals.update(quads)
madx_cols = 'Select,flag=twiss,column=name,keyword,s,l,betx,bety,dx, dpx, dy, dpy, alfx,alfy,x,y,k1l,mux,muy;'

madx.input(madx_cols)
#madx.input('savebeta, label=bpm, place=bpm.430039;')


madx.input('initbeta0:beta0,BETX=5,ALFX=0,DX=0,DPX=0,BETY=5,ALFY=0,DY=0.0,DPY=0.0,x=0,px=0,y=0,py=0;')
twiss_cpymad =madx.twiss(beta0='initbeta0').dframe()

#print(twiss_cpymad.betx['begi.1000'])
print(twiss_cpymad)

twiss_bpms = twiss_cpymad[twiss_cpymad['keyword']=='monitor']
twiss_correctors = twiss_cpymad[twiss_cpymad['keyword']=='kicker']
print(twiss_correctors.index.values.tolist())
twissH, twissV = twissReader.readAWAKEelectronTwiss()

betas = twissH.getBeta()
s_axis = twissH.getS()

betas_cpymad = twiss_cpymad['betx']
s_cpymad = twiss_cpymad['s']



import matplotlib.pyplot as plt

plt.figure()
plt.plot(s_cpymad,betas_cpymad)
plt.plot(s_axis,betas)
plt.show()